# platformio wokwi primer

## Setup

### vscode installieren

### platformio aufsetzen

1. Folgende Extension in vscode installieren: "platformio.platformio-ide"
2. Windows und Linux - [platformio Executable über die PATH Variable verfügbar machen](https://docs.platformio.org/en/stable/core/installation/shell-commands.html) 
3. Linux - [udev Rules und Gruppen Zuweisung](https://docs.platformio.org/en/latest/core/installation/udev-rules.html)
4. Dieses Projekt in einen Ordner kopieren und in vscode öffnen. die wokwi.toml muss im root Verzeichnis des Projektes sein
5. In der Command Line `pio run` ausführen
6. "Wokwi: Start Simulator" Commando in vscode ausführen (STRG+Shift+P)
7. Nach einer Änderung an der *main.cpp* einfach wieder `pio run` ausführen und die Änderungen sind im Simulator ersichtlich

### Wokwi aufsetzen

1. Folgende Extensions in vscode installieren: "Wokwi.wokwi-vscode"
2. STRG+Shift+P und dann "Wokwi: Request a new License" ausführen -  Schritte für die Registrierung (gratis) ausführen (bei Problemen gibt es [hier Details zu den notwendigen Schritten](https://docs.wokwi.com/vscode/getting-started))

### Projekt aufsetzen

1. Dieses Projekt in einen Ordner kopieren und in vscode öffnen. die wokwi.toml muss im root Verzeichnis des Projektes sein
2. In der Command Line `pio run` ausführen
3. "Wokwi: Start Simulator" Commando in vscode ausführen (STRG+Shift+P)
4. Nach einer Änderung an der *main.cpp* einfach wieder `pio run` ausführen und die Änderungen sind im Simulator ersichtlich

## Tipps

* Unter https://wokwi.com/ kann man sich anmelden und dort einen visuellen Editor für die Simulation verwenden. Die Daten aus der *diagram.json* müssen dann nur in die des Projektes geschrieben werden