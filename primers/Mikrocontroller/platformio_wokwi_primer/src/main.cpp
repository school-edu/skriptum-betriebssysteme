#include <Arduino.h>

int led_n = 3;
int start_led = 2;

void setup() {
    Serial.begin(9600);
    for(int i = start_led; i < start_led + led_n + 1; i++){
        pinMode(i, OUTPUT);
    }  
}

void loop() {
    for(int i = start_led; i < start_led + led_n + 1; i++){
        Serial.print("Power on Pin: ");
        Serial.println(i);
        digitalWrite(i, HIGH);  
        delay(1000);             
        digitalWrite(i, LOW);   
        delay(1000);       
    }  
}