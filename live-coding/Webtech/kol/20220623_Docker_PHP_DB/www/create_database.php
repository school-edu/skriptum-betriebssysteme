<?php

$host = "mysql";
$user = "root";
$pass = "123";
$database = "serp";

$message[] =  "";

$running = true;
while($running){
  try {
    $conn = new PDO('mysql:host='.$host.';charset=utf8', $user, $pass);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $running = false;
    $message[] =  "Connection established";
  }
  catch(PDOException $e) {
    $message[] =  "Connection to $host failed: " . $e->getMessage();
    sleep(1);
  }

}

/**
 * Select Database
 */
try {
    $conn->exec('USE '.$database);
    $message[] =  "Database $database existing and connected.";
  }
  catch(PDOException $e) {
    // if not possible to select, create Database
      $message[] = 'Database "'.$database.'" does not exist. Creating database ...';
      $sql = file_get_contents('database.sql');
      $qr = $conn->exec($sql);
    try {
      $conn->exec('USE '.$database);
      $message[] =  "Database $database existing and connected.";
    }
    catch(PDOException $e2) {
      $message[] =  "Setting up database failed: " . $e2->getMessage();
    }
  }

foreach($message as $line){
  echo "$line <br>";
}


?>

