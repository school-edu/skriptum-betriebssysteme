#!/bin/bash

ping -c 1 www.google.de &> /dev/null
success=$?

if [ $success -eq 0 ]; then
    echo "Wir haben Internetverbindung"
else
    echo "ERROR! Keine Internetverbindung!"
fi