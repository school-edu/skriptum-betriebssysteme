import java.util.ArrayList;
import java.util.List;

public class _01_join_with_race {
    public static void main(String[] args) {
        System.out.println("Start Program!");
        System.out.println(Thread.currentThread().getName());

        Runnable job_running = () -> {
            System.out.println(Thread.currentThread().getName());

            int nTimes = 1000;

            for (int i = 0; i < nTimes; i++) {
                Storage.counter = Storage.counter + 1;
            }
        };

        int nThreads = 10;
        List<Thread> tList = new ArrayList<>();

        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread(job_running,"Thread id "+i);
            tList.add(t);
            t.start();            
        }

        for (Thread t : tList) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Counting done: "+Storage.counter);
        
    }
}

class Storage {
    public static long counter = 0;
}