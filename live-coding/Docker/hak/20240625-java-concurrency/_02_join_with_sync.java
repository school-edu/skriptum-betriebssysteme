import java.util.ArrayList;
import java.util.List;

public class _02_join_with_sync {
    public static void main(String[] args) {
        System.out.println("Start Program!");
        System.out.println(Thread.currentThread().getName());

        int nThreads = 10;
        List<Thread> tList = new ArrayList<>();

        Runnable job = new JobRunning();

        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread(job,"Thread id: "+i);
            tList.add(t);
            t.start();
        }

        for (Thread t : tList) {
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Counting done: "+Storage2.counter);
        
    }
}

class Storage2 {
    public static long counter = 0;
}

class JobRunning implements Runnable {
    @Override
    public void run(){
        System.out.println(Thread.currentThread().getName());

        int nTimes = 1000;

        for (int i = 0; i < nTimes; i++) {
            countJob();
        }
    }

    synchronized void countJob(){
        Storage2.counter = Storage2.counter + 1;
    }
    
}