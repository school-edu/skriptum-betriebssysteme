FROM openjdk:8
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
RUN javac Main.java
RUN javac Main2.java
# Alternativ CMD - kann überschrieben werden
# CMD ["java"]
# Bei ENTRYPOINT werden die Optionen angehängt
ENTRYPOINT ["java"]