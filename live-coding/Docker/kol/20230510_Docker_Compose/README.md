## Starten einer Wordpress Instanz ohne `docker compose`

```bash
# Vorhande Images anzeigen (mit grep filtern was man sucht)
docker image ls
# Vorhandene Netzwerke anzeigen
docker network ls
# Ein Netzwerk erstellen in dem Container kommunizieren können
docker network create mein_wordpress_netzwerk
# Wordpress starten
docker run wordpress:6.2.0
# Port nach außen freigeben
docker run --rm -p 8080:80 wordpress:6.2.0
# Alle Details zum Container anschauen (welches Netzwerk wird genutzt?)
docker container inspect CONTAINER_NAME
# Container im das erstellten Netzwerk starten
docker run --rm -p 8080:80 --network "mein_wordpress_netzwerk" -d --name wordpress wordpress:6.2.0
# Gestoppte Container löschen
docker container prune
# Nicht gebrauchte Images löschen
docker image prune -ad
# MYSQL starten
docker run --rm --name mysql --network mein_wordpress_netzwerk -e "MYSQL_DATABASE=exampledb" -e "MYSQL_USER=exampleuser" -e "MYSQL_PASSWORD=examplepass" -e "MYSQL_RANDOM_ROOT_PASSWORD=1" -d mysql:5.7
# Cotainer Logs anzeigen
docker logs -t mysql
# docker compose starten
docker compose up
# im Hintergrund
docker compose up -d
# Alle Container der docker compose stoppen
docker compose stop
# Alle Container löschen
docker compose down
```