```bash
docker --help
docker run --help
docker run -p 8888:80 --rm -d httpd
docker --help
docker container --help
docker container ls
docker image --help
docker image ls
# Verbindung in einen laufenden Container -> 6e7 ist Container id siehe `docker image ls`
docker exec -ti 6e78227699b6 bash
# starten eines Webservers und verbinden eines Volumes mit dem Document Root auf den Host
docker run -p 8899:80 --rm -d -v ./httpd-volume:/usr/local/apache2/htdocs httpd
# Starte zwei verschiedene Webserver und mache auf beiden eine unterschiedliche Website Version. Einmal per Volume änderbar und einmal per Direktverbindung mit `docker exec -ti` siehe oben.
```