## GNS3 Installation

* [Linux Installation](https://docs.gns3.com/docs/getting-started/installation/linux/)
* [Windows Installation](https://gns3.com/software/download)
* [VMware Workstation](https://www.gns3.com/software/download-vm)

## dbDocker docker-compose

* [dbDocker Vorlage](https://gitlab.com/st.stolz/Skills-Project/-/tree/master/dockerSkills/dbDocker)