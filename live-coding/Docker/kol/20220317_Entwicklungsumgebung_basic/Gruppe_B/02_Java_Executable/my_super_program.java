import java.util.Random;
import java.util.Scanner;

public class my_super_program {
    public static void main(String[] args) {

        System.out.println("Erstes Argument: "+args[0]);

        Scanner scanner = new Scanner(System.in);
        boolean running = true;

        Random rand = new Random();
        int zuErraten = rand.nextInt(1000)+1;

        while(running){
            System.out.println("Rate eine Zahl zwischen 1 und 1000: ");
            int geraten = Integer.parseInt(scanner.nextLine());
            if(geraten < zuErraten){
                System.out.println("Zu klein geraten");
            }
            else if (geraten > zuErraten){
                System.out.println("Zu groß geraten");
            }
            else {
                System.out.println("Gewonnen!");
                running = false;
            }
        }
    }
}