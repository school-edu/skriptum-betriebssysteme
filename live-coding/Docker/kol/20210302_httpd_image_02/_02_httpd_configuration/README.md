Eine Datei aus einem Container auf den Host kopieren:

`docker run --rm imageName cat pfadImImage > dateiNameAmHost`

Beispiel für httpd aus dem [offiziellen README](https://hub.docker.com/_/httpd)