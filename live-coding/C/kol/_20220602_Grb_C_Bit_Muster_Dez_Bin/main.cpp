#include <stdio.h>

void print_bits(int data){

    for(int i=0; i < 8;i++){
        int zahl_shifted = data >> i;
        int is_bit_set = zahl_shifted & 1;
        printf("%d",is_bit_set);
    }

}

int main(){

    int zahl = 5;
    printf("Die Zahl: %d \n",zahl);

    zahl = zahl << 1;
    printf("Die Zahl: %d \n",zahl);

    zahl = zahl >> 1;
    printf("Die Zahl: %d \n",zahl);

    printf("Die Zahl: %d \n",zahl&1);

    zahl = zahl >> 1;

    printf("Die Zahl: %d \n",zahl&1);

    zahl = zahl >> 1;

    printf("Die Zahl: %d \n",zahl&1);

    zahl = zahl >> 1;

    printf("Die Zahl: %d \n",zahl&1);

    return 0;
}