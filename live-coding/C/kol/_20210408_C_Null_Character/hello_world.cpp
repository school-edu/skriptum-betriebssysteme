#include<stdio.h>
#include <string.h>

int main() {
	printf("Hello World\n");

	char char_array[6] = {'s', 't', 'r', 'i', 'n', 'g'};

	for(int i = 0; i<6; i++){
		printf("%.*s", 1, &char_array[i]);
	}
	printf("\n");

	char str_array[7];

	memset(str_array, '\0', sizeof(str_array));
	strncpy(str_array, char_array, 6);

	printf(str_array);

	printf("\n");

	return 0;
}