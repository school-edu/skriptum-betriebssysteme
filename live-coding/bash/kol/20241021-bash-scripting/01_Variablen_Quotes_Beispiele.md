```bash
var1="Hallo Welt"
var2='Hallo Welt'
echo $var1
echo $var2
var2=Hallo Welt # Erzeugt Fehler. Warum?
var2=Hallo\ Welt
var1="Hallo"
echo "$var1 Stefan"
echo '$var1 Stefan'
echo "\$var1 Stefan"
echo $var1Stefan
echo ${var1}Stefan
var3="Die Uhrzeit ist: date"
echo $var3
var3="Die Uhrzeit ist: $(date)"
echo $var3
```