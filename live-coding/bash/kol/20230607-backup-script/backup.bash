#!/bin/bash

backup_from="./zu_sicherende_Daten"
backup_to="./backup_Ordner"
remote_backup_to="~/backup"

current_timestamp=$(date +%Y%m%d-%H%M%S)

echo "Starting backup at ${current_timestamp}"

if [ ! -d ${backup_to} ] ;  then
    echo "Directory does not exist. Creating it..."
    mkdir -p ${backup_to}
fi

files_to_delte=$(find ${backup_to} -maxdepth 1 -type f -mmin +1 -name '*backup*')

for i in $files_to_delte
do
    rm $i
done 

echo "Führe Sicherung von ${backup_from} nach ${backup_to} durch"

tar -cf ${backup_to}/${current_timestamp}-backup.tar $backup_from



if ssh ststolz@192.168.56.21 test -d ${remote_backup_to}; then

    echo "Remote Verzeichnis vorhanden"
    
    files_to_backup_remote=$(find ${backup_to} -maxdepth 1 -type f -name '*backup*')

    for i in $files_to_backup_remote
    do
        scp $i ststolz@192.168.56.21:${remote_backup_to}
    done 


else

    echo "Remote Verzeichnis muss erstellt werden"
    ssh ststolz@192.168.56.21 mkdir -p ${remote_backup_to}

fi