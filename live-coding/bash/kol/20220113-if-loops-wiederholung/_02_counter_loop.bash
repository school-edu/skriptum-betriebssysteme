#!/bin/bash

counter=0

while [ $counter -lt 10 ]; do

    echo "Aufruf: "`expr $counter + 1`
    counter=`expr $counter + 1`

done

# Alternativ mit for Loop
for ((i=0;i<10;i++)); do

    echo "For Aufruf: "`expr $i + 1`

done