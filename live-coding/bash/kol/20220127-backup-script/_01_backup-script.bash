#!/bin/bash

date_filename=$(date +%Y%m%d-%H%M%S)
#echo $date_filename

tar -czf ~/tmp/backup/${date_filename}_backup.tar ~/tmp/files_to_backup

for i in $(find ~/tmp/backup -maxdepth 1 -mmin +1 -type f); do 

    echo "removing file: "$i
    rm $i

done

# find ~/tmp/ -maxdepth 1 -mmin +1 -type f -delete

# find ~/tmp/ -maxdepth 1 -mmin +1 -type f -exec rm {} \;

for i in $(find ~/tmp/backup -maxdepth 1 -type f); do 

    echo "transfer file to remote: "$i
    scp $i ststolz@192.168.56.254:backupfolder

done