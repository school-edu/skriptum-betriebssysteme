#!/bin/bash

backup_from="./zu_sicherende_Daten"
backup_to="./backup_Ordner"

current_timestamp=$(date +%Y%m%d-%H%M%S)

echo "Starting backup at ${current_timestamp}"

if [ ! -d ${backup_to} ] ;  then
    echo "Directory does not exist. Creating it..."
    mkdir -p ${backup_to}
fi

files_to_delte=$(find ./backup_Ordner/ -maxdepth 1 -type f -mmin +1 -name '*backup*')

for i in $files_to_delte
do
    rm $i
done 

echo "Führe Sicherung von ${backup_from} nach ${backup_to} durch"

tar -cf ${backup_to}/${current_timestamp}-backup.tar $backup_from