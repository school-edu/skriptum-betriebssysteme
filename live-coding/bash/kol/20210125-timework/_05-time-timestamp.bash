#!/bin/bash

job () {
    echo "Executing Job"
    sleep 1
}

startTime=`date +%s`
now=startTime
lastRun=startTime

for ((i=0;i<5;i++)); do    
    lastRun=`date +%s`
    job
    now=`date +%s`
    executionTime=$((now-lastRun))
    echo "Execution time: $executionTime"
done

now=`date +%s`

overallExecutionTime=$((now-startTime))

echo "Overall Execution time: $overallExecutionTime"