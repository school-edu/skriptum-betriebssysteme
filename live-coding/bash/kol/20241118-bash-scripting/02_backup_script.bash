#!/bin/bash

to_backup="./to_backup"
backup_to="./backup_dir2"
date_string=$(date +%Y%m%d-%H%M%S)

echo "Backup Start Zeitpunkt:" ${date_string}

# Archivdatei für Backup anlegen
tar -cf ${backup_to}/${date_string}-archive.tar ${to_backup}

# Inhalt der Archivdatei als Check ausgeben
# Aufgabe 1: Beim folgenden Befehl soll das richtige, gerade angelegte Verzeichnis geprüft werden
# Aufgabe 2: Beim folgenden Befehl soll die Fehlerausgabe verhindert werden
tar -tvf ${backup_to}/archive.tar

# Aufgabe 3: If Bedingung, die bei fehlerhafter tar Ausführung Custom Fehler ausgibt und ansonsten Erfolgsmeldung

# Aufgabe 4: Falls Fehler, mit >0 exit

# Aufgabe 5: Baue das Beispiel 01_example_file_loop.bash in das Script ein, sodass dein Script in Folge alle Dateien löscht, die älter als 30 Sekunden sind

# Aufgabe 6: 01_example_file_loop.bash erklären können