#!/bin/bash

# Verzeichnis, das durchsucht werden soll
directory="/pfad/zum/ordner"

# Liste der Dateien, die älter als 30 Sekunden sind
file_list=$(find "$directory" -type f -mmin +0.5)

# Schleife über jede Datei in der Liste
for file in $file_list; do
    # Letzter Änderungszeitpunkt der Datei in Sekunden seit Epoch
    file_mod_time=$(stat --format=%Y "$file")

    # Aktuelle Zeit in Sekunden seit Epoch
    current_time=$(date +%s)

    # Differenz berechnen
    time_diff=$((current_time - file_mod_time))

    # Ergebnis ausgeben
    echo "Datei: $file - Differenz: ${time_diff} Sekunden"
done