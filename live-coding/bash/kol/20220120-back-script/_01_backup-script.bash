#!/bin/bash

date_filename=`date +%Y%m%d-%H%M%S`

tar -czf ~/tmp/${date_filename}_backup.tar ~/tmp/backupdir