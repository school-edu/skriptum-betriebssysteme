#!/bin/bash

echo "Starte Backup Script"

# alternative Schreibweise
# datestring=`date "+%Y%m%d%H%M%S"`
datestring=$(date "+%Y%m%d%H%M%S")

backupfile="/home/ststolz/git/skriptum-betriebssysteme/live-coding/bash/hak/20231212_backup_script/Backup.md"
backuptofolder="/home/ststolz/git/skriptum-betriebssysteme/live-coding/bash/hak/20231212_backup_script/backup/"
backuptofile="${datestring}_backup.md"



cp ${backupfile} ${backuptofolder}${backuptofile}