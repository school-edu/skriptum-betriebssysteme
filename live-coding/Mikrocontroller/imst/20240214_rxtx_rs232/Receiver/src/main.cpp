#include <Arduino.h>

 //Initialized variable to store recieved data

void setup() {
  // Begin the Serial at 9600 Baud
  Serial.begin(9600);
  delay(1000);
  Serial.println("Setup ready");
}

void loop() {
  char mystr[10]="";
  Serial.println("Enter data!");
  while (Serial.available() == 0) {}     //wait for data available
  Serial.readBytes(mystr, 6);             // remove any \r \n whitespace at the end of the String
  Serial.println(mystr);
  delay(1000);
}