#include <Arduino.h>

uint16_t payload = 0;

void setup()
{
  Serial.begin(9600);
  delay(500);

  pinMode(14, OUTPUT);
  pinMode(15, OUTPUT);
}

void receive_uart(){
  byte* buffer = (byte*)malloc(2*sizeof(*buffer));
  Serial.readBytes(buffer, 2);
  memcpy(&payload,buffer,sizeof(payload));
}


void loop()
{
  if (Serial.available() > 0) {
    receive_uart();
    delay(500);

    for (int i = 0; i < payload; i++)
    {
      digitalWrite(14, HIGH);
      delay(500);
      digitalWrite(14, LOW);
      delay(500);
    }

    digitalWrite(14, HIGH);
    digitalWrite(15, HIGH);
    delay(50);
    digitalWrite(14, LOW);
    digitalWrite(15, LOW);
  }

  
}