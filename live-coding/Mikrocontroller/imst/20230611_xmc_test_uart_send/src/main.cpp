#include <Arduino.h>

uint16_t payload = 0;

void setup()
{
  Serial.begin(9600);
  // Serial.println("pas co2 serial initialized");
  delay(500);

  pinMode(14, OUTPUT);
  pinMode(15, OUTPUT);
}

void send_uart()
{
  byte *payload_byte = (byte *)malloc(2* sizeof(*payload_byte));
  memcpy(payload_byte,&payload,2*sizeof(payload_byte));
  Serial.write(payload_byte, 2);
}

void loop()
{
  payload++;

  for (int i = 0; i < payload; i++)
  {
    digitalWrite(14, HIGH);
    delay(500);
    digitalWrite(14, LOW);
    delay(500);
  }

  send_uart();

  digitalWrite(14, HIGH);
  digitalWrite(15, HIGH);
  delay(2000);
  digitalWrite(14, LOW);
  digitalWrite(15, LOW);
}