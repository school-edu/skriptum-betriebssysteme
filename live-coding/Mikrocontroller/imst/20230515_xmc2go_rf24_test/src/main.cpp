#include <Arduino.h>

#include "RF24.h"
#include "RF24Network.h"
#include "RF24Mesh.h"
#include <SPI.h>

int led_n = 3;
int start_led = 2;

void setup() {
    Serial.begin(9600);
    pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
    
    Serial.print("Power on Pin: ");
    Serial.println(LED_BUILTIN);
    digitalWrite(LED_BUILTIN, HIGH);  
    delay(1000);             
    digitalWrite(LED_BUILTIN, LOW);   
    delay(1000);       
    
}