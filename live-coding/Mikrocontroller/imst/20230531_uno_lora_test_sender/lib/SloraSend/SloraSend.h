#ifndef SloraSend
#define SloraSend

#include <Arduino.h>

#define DEBUG_LEVEL 1
#define DEBUG_INFO_SERIAL if(DEBUG_LEVEL > 1)Serial

#define DEBUG_WARNING true
#define DEBUG_WARNING_SERIAL if(DEBUG_LEVEL > 0)Serial

struct DataPacket {
  uint32_t hash;
  uint32_t millis;
  uint16_t sensors_id;
  uint16_t sensing_purposes_id;
  uint32_t value;
};
extern DataPacket packet;

// SloraSend Settings
extern const uint16_t SENSORS_ID;

// set in file "SloraSend_keys.cpp"
extern const char* NONCE;

void setup_slorasend();

int send_with_conf(uint32_t, uint16_t);

#endif /* SloraSend */