#include <Arduino.h>

#include "SloraSend.h"

#include <RadioLib.h>
#include <xxh3.h>

// SX1272 has different connections:
// NSS pin:   9
// DIO0 pin:  4
// RESET pin: 5
// DIO1 pin:  6

// RFM95 radio = new Module(10, 3, 5, 6);

// XMC 2GO:
// NSS pin:   3
// DIO0 pin:  4
// RESET pin: 8
// DIO1 pin:  5
RFM95 radio = new Module(3, 4, 8, 5);

DataPacket packet;

void setup_slorasend() {      

  // initialize RFM95
  DEBUG_WARNING_SERIAL.print(F("[RFM95] Initializing ... "));
  int state = radio.begin(868.0, 125.0, 12, 8, 0x34, 20, 8, 1);
  if (state == RADIOLIB_ERR_NONE) {
    DEBUG_WARNING_SERIAL.println(F("success!"));
  } else {
    DEBUG_WARNING_SERIAL.print(F("failed, code "));
    DEBUG_WARNING_SERIAL.println(state);
    while (true);
  }

}

int send_with_conf(uint32_t payload, uint16_t sensing_purposes_id){

    // Output simple hex String

    // DEBUG_INFO_SERIAL.print("SHA1 Hex Str:");
    // DEBUG_INFO_SERIAL.println(sha1(payload));

    // Produce Byte Array

    // const char* data = str_to_hash.c_str();
    
  
  // *(data_packet + 0) = XXH32(&payload, sizeof(payload), 0);
  // *(data_packet + 4) = millis();
  // *(data_packet + 8) = 13;
  // *(data_packet + 9) = payload;
  // packet.hash = *(data_packet + 0);
  // packet.millis =  *(data_packet + 4);
  // packet.room = *(data_packet + 8);
  // packet.val = *(data_packet + 9);

  packet.millis = millis();
  packet.sensors_id = SENSORS_ID;
  packet.sensing_purposes_id = sensing_purposes_id;
  packet.value = payload;

  char* hashload;
  int size_of_hashload = sizeof(*hashload)*(strlen(NONCE)+1+sizeof(packet.value)+sizeof(packet.millis)+sizeof(packet.sensors_id)+sizeof(packet.sensing_purposes_id));

  DEBUG_INFO_SERIAL.print(F("size_of_hashload:\t\t\t"));
  DEBUG_INFO_SERIAL.println(size_of_hashload);

  hashload = (char*) malloc(size_of_hashload);
  // strcpy(hashload, pay_byte_arr);
  // strcat(hashload, nonce);
  sprintf(hashload, "%s%lu%lu%u%u", NONCE, packet.value, packet.millis, packet.sensors_id, packet.sensing_purposes_id);

  DEBUG_INFO_SERIAL.print(F("real_size_of_hashload:\t\t\t"));
  DEBUG_INFO_SERIAL.println(strlen(hashload));

  DEBUG_INFO_SERIAL.print(F("hashload:\t\t\t"));
  DEBUG_INFO_SERIAL.println(hashload);

  packet.hash = XXH32(hashload, strlen(hashload), 0);
  // const char *hashload_test = "Hall";
  // packet.hash = XXH32(packet.hash, strlen(packet.hash), 0);

  free(hashload);
  

  DEBUG_INFO_SERIAL.print(F("Size:\t\t\t"));
  DEBUG_INFO_SERIAL.println(sizeof(packet));

  DEBUG_INFO_SERIAL.print(F("Hash:\t\t\t"));
  DEBUG_INFO_SERIAL.println(packet.hash, HEX);

  DEBUG_INFO_SERIAL.print(F("Millis:\t\t\t"));
  DEBUG_INFO_SERIAL.println((unsigned long)packet.millis);

  DEBUG_INFO_SERIAL.print(F("Room:\t\t\t"));
  DEBUG_INFO_SERIAL.println(packet.sensors_id);

  DEBUG_INFO_SERIAL.print(F("Val:\t\t\t"));
  DEBUG_INFO_SERIAL.println(packet.value);


  int state = radio.transmit(reinterpret_cast <uint8_t *> (&packet), sizeof(packet));

  // free(pay_byte_arr);

  // you can also transmit byte array up to 256 bytes long
  /*
    byte byteArr[] = {0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF};
    int state = radio.transmit(byteArr, 8);
  */

  if (state == RADIOLIB_ERR_NONE) {
    // the packet was successfully transmitted
    DEBUG_WARNING_SERIAL.print(F("["));
    DEBUG_WARNING_SERIAL.print(packet.millis);
    DEBUG_WARNING_SERIAL.print(F("]\t"));
    DEBUG_WARNING_SERIAL.println(F("Packet successfully transmitted!"));

    // print measured data rate
    DEBUG_INFO_SERIAL.print(F("[RFM95] Datarate:\t"));
    DEBUG_INFO_SERIAL.print(radio.getDataRate());
    DEBUG_INFO_SERIAL.println(F(" bps"));

  } else if (state == RADIOLIB_ERR_PACKET_TOO_LONG) {
    // the supplied packet was longer than 256 bytes
    DEBUG_WARNING_SERIAL.println(F("too long!"));

  } else if (state == RADIOLIB_ERR_TX_TIMEOUT) {
    // timeout occurred while transmitting packet
    DEBUG_WARNING_SERIAL.println(F("timeout!"));

  } else {
    // some other error occurred
    DEBUG_WARNING_SERIAL.print(F("failed, code "));
    DEBUG_WARNING_SERIAL.println(state);

  }
  return state;

}

int receive_conf(){
  return 1;
  
}