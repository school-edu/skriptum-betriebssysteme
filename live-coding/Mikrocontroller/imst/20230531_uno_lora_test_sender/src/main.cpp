#include <Arduino.h>
#include <RadioLib.h>
#include <pas-co2-ino.hpp>
#include <pas-co2-platf-ino.hpp>
#include <xensiv_pasco2.h>
#include <xensiv_pasco2_regs.h>
#include <xensiv_pasco2_ver.h>

#include "SloraSend.h"

// SloraSend Settings
const uint16_t SENSORS_ID = 2;

uint16_t package_count = 0;

/* 
 * The sensor supports 100KHz and 400KHz. 
 * You hardware setup and pull-ups value will
 * also influence the i2c operation. You can 
 * change this value to 100000 in case of 
 * communication issues.
 */
#define I2C_FREQ_HZ  400000                     
#define PERIODIC_MEAS_INTERVAL_IN_SECONDS  10 
#define PRESSURE_REFERENCE  900

/*
 * Create CO2 object. Unless otherwise specified,
 * using the Wire interface
 */
PASCO2Ino cotwo;

int16_t co2ppm;
Error_t err;

void setup() {
  Serial.begin(9600);

  setup_slorasend();

  delay(800);

  /* Initialize the i2c interface used by the sensor */
    Wire.begin();
    Wire.setClock(I2C_FREQ_HZ);

    /* Initialize the sensor */
  
}

void loop() {
  DEBUG_INFO_SERIAL.println(F("[SX1278] Transmitting packet ... "));

  // you can transmit C-string or Arduino string up to
  // 256 characters long
  // NOTE: transmit() is a blocking method!
  //       See example SX127x_Transmit_Interrupt for details
  //       on non-blocking transmission method.
  package_count++;

  // char buffer[10];  
  // sprintf(buffer, "%d", package_count);

  send_with_conf(package_count, 2);

  // wait for a second before transmitting again
  delay(1000);


}