#include <Arduino.h>

int led = LED_BUILTIN;

void setup(){
    Serial.begin(9600);
    pinMode(led, OUTPUT);
}

void loop(){

    digitalWrite(led,HIGH);
    int value = digitalRead(led);
    Serial.print("HIGH: ");
    Serial.println(value);

    delay(1000);

    digitalWrite(led,LOW);
    value = digitalRead(led);
    Serial.print("LOW: ");
    Serial.println(value);

    delay(1000);
    
}