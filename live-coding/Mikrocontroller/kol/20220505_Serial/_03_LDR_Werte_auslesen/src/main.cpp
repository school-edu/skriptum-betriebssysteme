#include <Arduino.h>

int ldr_pin = A0;

void setup(){
    Serial.begin(9600);
}

void loop(){
    int val = analogRead(ldr_pin);
    Serial.println(val);
    delay(1000);
}