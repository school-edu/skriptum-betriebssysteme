#include <Arduino.h>

void setup(){
    // ACHTUNG! Baud Rate muss auch in der platformio.ini mitgeteilt werden, wenn von 9600 abweicht
    Serial.begin(14400);
}

void loop(){
    Serial.println("Hello World");
    delay(1000);
}