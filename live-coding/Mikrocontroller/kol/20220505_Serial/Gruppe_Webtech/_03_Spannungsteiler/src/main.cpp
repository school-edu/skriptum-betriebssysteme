#include <Arduino.h>

int ldr = A0;

void setup(){
    // ACHTUNG! Baud Rate muss auch in der platformio.ini mitgeteilt werden, wenn von 9600 abweicht
    Serial.begin(9600);
}

void loop(){
    int value = analogRead(ldr);
    Serial.println(value);
    delay(1000);
}