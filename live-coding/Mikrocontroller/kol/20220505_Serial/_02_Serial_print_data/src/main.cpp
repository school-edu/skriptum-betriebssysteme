#include <Arduino.h>

// Blink Programm mit zusätzlichem auslesen des am LED Pin gesetzten Wertes
// Und zurückschreiben des Wertes per Serial

int led = LED_BUILTIN;

void setup() {
  pinMode(led, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  digitalWrite(led,HIGH);
  int pin_val = digitalRead(led);
  Serial.print("HIGH: ");
  Serial.println(pin_val);
  delay(1000);
  digitalWrite(led,LOW);
  pin_val = digitalRead(led);
  Serial.print("LOW: ");
  Serial.println(pin_val);
  delay(1000);
}