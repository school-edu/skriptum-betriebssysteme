#include <Arduino.h>

int ledRedPin = 12;
int ledGreenPin = 13;

void setup() {
  pinMode(ledGreenPin,OUTPUT);
  pinMode(ledRedPin,OUTPUT);
}

void loop() {
  digitalWrite(ledGreenPin,HIGH);
  delay(1000);
  digitalWrite(ledGreenPin,LOW);
  delay(1000);
  digitalWrite(ledRedPin,HIGH);
  delay(200);
  digitalWrite(ledRedPin,LOW);
  delay(200);
}