#include <Arduino.h>

byte dataPin = 11;
byte stcPin = 8;
byte shcPin = 12;


void setup() {
  pinMode(dataPin, OUTPUT);
  pinMode(stcPin, OUTPUT);
  pinMode(shcPin, OUTPUT);
  digitalWrite(dataPin,LOW);
  digitalWrite(stcPin,LOW);
  digitalWrite(shcPin,LOW);
}

void loop() {
      for(byte i = 0; i <= 255; i++){
        progShiftReg(i);
        delay(1000);       
      }  
}

void progShiftReg(byte data){
  for(byte i = 0; i < 8; i++){
    byte mask = 1 << i;
    byte masked_data = data & mask;
    byte writeData = masked_data >> i;
    digitalWrite(dataPin,writeData);

    digitalWrite(shcPin,HIGH);
    digitalWrite(shcPin,LOW);
  }
  digitalWrite(stcPin,HIGH);
  digitalWrite(stcPin,LOW);
}
