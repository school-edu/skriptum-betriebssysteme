#include <Arduino.h>
#include <limits.h>

// In C gibt es unsigned, wodurch man den gesamten positiven Bereich ausnutzen kann
unsigned int variblenname_uint;
int variblenname_int;

void setup() {
  Serial.begin(9600);
  
}

void loop() {
  variblenname_uint = -1;
  variblenname_int = 32768;
  Serial.print("INT_MIN: ");
  Serial.print(INT_MIN);
  Serial.print(", INT_MAX: ");
  Serial.print(INT_MAX);
  Serial.print(", UINT_MAX: ");
  Serial.println(UINT_MAX);
  Serial.print("UINT -1 = ");
  Serial.println(variblenname_uint);
  Serial.print("INT 32768 = ");
  Serial.println(variblenname_int);
  delay(1000);  
}