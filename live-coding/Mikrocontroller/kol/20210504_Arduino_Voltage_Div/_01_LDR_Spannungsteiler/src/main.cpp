#include <Arduino.h>

int greenLed = 13;
int analogPin = A1; // Pin, der gelesen werden soll: Pin A3
int val = 0; // Variable, die den gelesenen Wert speichert

void setup() {
  pinMode(greenLed,OUTPUT);
  Serial.begin(9600); // Setup der seriellen Verbindung
}

void loop() {
  val = analogRead(analogPin); // Pin einlesen
  Serial.println(val); // Wert ausgeben
  if(val > 500){
    digitalWrite(greenLed,HIGH);
  }
  else{
    digitalWrite(greenLed,LOW);
  }
}