#include <Arduino.h>

#define BLUELED 12
#define YELLOWLED 8


int ledStateBlue = LOW; 
int ledStateYellow = LOW;


unsigned long previousMillis1 = 0;
unsigned long previousMillis2 = 0;


const long interval1 = 1000; 
const long interval2 = 200;

void setup()
{
  pinMode(BLUELED, OUTPUT);
  pinMode(YELLOWLED, OUTPUT);
}

void loop()
{

  unsigned long currentMillis1 = millis();
  // Zweites currentMillis ist unnötig, weil currentMillis1 nicht verändert wird
  unsigned long currentMillis2 = millis();

  if (currentMillis1 - previousMillis1 >= interval1)
  {
    previousMillis1 = currentMillis1;
    if (ledStateBlue == LOW)
    {
      ledStateBlue = HIGH;
    }
    else
    {
      ledStateBlue = LOW;
    }
    digitalWrite(BLUELED, ledStateBlue);
  }

  if (currentMillis2 - previousMillis2 >= interval2)
  {
    previousMillis2 = currentMillis2;
    if (ledStateYellow == LOW)
    {
      ledStateYellow = HIGH;
    }
    else
    {
      ledStateYellow = LOW;
    }
    digitalWrite(YELLOWLED, ledStateYellow);
  }
}
