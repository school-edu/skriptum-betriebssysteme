int led_start = 2;
int led_n = 3;

void setup() {
  for(int i = led_start; i < led_n+led_start; i++){
    pinMode(i, OUTPUT);   
  }
  
  Serial.begin(9600);
    
}

void loop() {
  
  for(int i = led_start; i < led_n+led_start; i++){
    digitalWrite(i, HIGH);   
    delay(1000);               
    digitalWrite(i, LOW);    
    delay(1000);  
  }
  
  int ldr_val = analogRead(A0);
  Serial.println(ldr_val);
}
